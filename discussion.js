//[SECTION] MONGODB AGGREGATION 
	//  mongo db aggregation is used to generate and perform operations of create filtered results that helps us analyze the data.

	/*
		Using agregate method:
		-the "$match" is used to pass the documnets that meet the specified condition/s to the next stage or aggregation process
		Syntax:
			{$match:{field: value}}
	*/

	db.fruits.aggregate([{$match:{onSale:true}}]);

	//Using the group the elements together and field value pairs the data from the groupes element
		// Syntax:
		//{$group: _id:"filedSetGroup"};

		db.fruits.aggregate([
			{$match:{onSale:true}},
			{$group: {_id:"$supplier_id", totalFruits;{$sum: "$stock"}}}
			]);

		db.fruits.aggregate([
			{$match:{color:"Yellow"}},
			{$group:{_id:"$supplier_id", totalStocks:{$sum:"$stock"}}}
			]);		

	// Field Projection with aggregation
		/*
			-$project can be used when aggregation data to include/exclude from the returned result
				Syntax:
					{$project:{field:1/0}}

		*/

		db.fruits.aggregate([
			{$match:{onSale:true}},
			{$group: {_id:"$supplier_id", totalFruits;{$sum: "$stock"}}}
			{$project: {_id:0}}
			]);		
		
		// Sortinmg aggregated result
		/*
			$sort can be used to change the order of the aggregated result
		*/

			db.fruits.aggregate([
			{$match:{onSale:true}},
			{$group: {_id:"$supplier_id", totalFruits;{$sum: "$stock"}}}
			{$project: {_id:0}},
			{$sort:{totalFruits:1}}
			]);		

			//The value in sort 
				// 1 - the lowest to highest
				// -1 - the highest to lowest

		//Aggregating based on an array fields
		/*
			yhe $unwind deconstructs an array field from a collection / field with an array value to output a result 
		*/ 
			db.fruits.aggregate([
				{$unwind:"$origin"},
				{$group:{_id:"$origin"}}]);


		// Other Aggregate stages
			// $counts all the yellow fruits
			db.fruits.aggregate([
				{$match:{color:"Yellow"}},
				{$count:"Yellow Fruits"}])


		//$avg gets the average value of the stock 

			db.fruits.aggregate([
				{$match:{color:"Yellow"}},
				{$group:{_id:"$color", avgYellow:{$avg:"$stock"}}}
				]);

		// $min && $max

			db.fruits.aggregate([
				{$match:{color:"Yellow"}},
				{$group:{_id:"$color", lowestStock:{$min:"$stock"}}}
				]);

			db.fruits.aggregate([
				{$match:{color:"Yellow"}},
				{$group:{_id:"$color", highestStock:{$max:"$stock"}}}
				]);